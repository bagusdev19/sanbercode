<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Day 1 Bootcamp</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <!-- Form -->
    <form action="/kirim" method="POST">
        @csrf
        <label for="fname">First name:</label> <br>
        <input type="text" name="fname"> <br> <br>
        <label for="lname">Last name:</label> <br>
        <input type="text" name="lname"> <br> <br>
        <label for="gender">Gender</label> <br> 
        <input type="radio" name="gender" value="male">Male <br>
        <input type="radio" name="gender" value="female">Female <br>
        <input type="radio" name="gender" value="other">Other <br><br>
        <label for="">Language Spoken</label> <br>
        <input type="checkbox" name="language"> Bahasa <br> 
        <input type="checkbox" name="language"> English <br> 
        <input type="checkbox" name="language"> Other <br> <br>
        <label for="bio">Bio:</label> <br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>