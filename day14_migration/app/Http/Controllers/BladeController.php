<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BladeController extends Controller
{
    public function dashboard()
    {
        return view('halaman.dashboard');
    }

    public function datatables()
    {
        return view('halaman.data-tables');
    }
    
    public function table()
    {
        return view('halaman.table');
    }
}
