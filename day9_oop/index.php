<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

echo "<h3>output akhir</h3>";

$sheep = new Animal("shaun");

echo "Name : ".$sheep->name. "<br>"; // "shaun"
echo "Legs : ".$sheep->legs. "<br>"; // 4
echo "cold blooded : ".$sheep->cold_blooded. "<br><br>"; // "no"

$kodok = new Frog("buduk");
echo "Name : ".$kodok->name. "<br>"; 
echo "Legs : ".$kodok->legs. "<br>"; 
echo "cold blooded : ".$kodok->cold_blooded. "<br>"; 
echo "Jump : ";
$kodok->jump() ; // "hop hop"

$sungokong = new Ape("kera sakti");
echo "<br><br>Name : ".$sungokong->name. "<br>"; 
echo "Legs : ".$sungokong->legs. "<br>"; 
echo "cold blooded : ".$sungokong->cold_blooded. "<br>"; 
echo "Yell : ";
$sungokong->yell();



// $sungokong = new Ape("kera sakti");
// $sungokong->yell() // "Auooo"

// $kodok = new Frog("buduk");
// $kodok->jump() ; // "hop hop"

?>